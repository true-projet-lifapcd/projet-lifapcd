#ifndef _JEUTXT_H
#define _JEUTXT_H

#include "Deplacement.h"

/**
* @brief Récupère la saisie clavier et actualise l'affichage textuel en conséquence.
**/
void txtBoucle(Deplacement & depla);

#endif
