#include "Monde.h"

Monde::Monde(){
    mX=2;
    mY=5;
    mCarte=9;


//_____________PNJ_PLACEMENT_________________________


tabCarte[5].tabCase[1][7].pnj=true;
tabCarte[1].tabCase[5][0].pnj=true;
tabCarte[1].tabCase[4][7].pnj=true;
tabCarte[3].tabCase[1][0].pnj=true;
tabCarte[3].tabCase[4][2].pnj=true;
tabCarte[7].tabCase[6][4].pnj=true;
tabCarte[7].tabCase[6][8].pnj=true;

//_____________PNJ_PLACEMENT_________________________

tabCarte[8].tabCase[4][1].coffre=true;
tabCarte[9].tabCase[3][2].coffre=true;
tabCarte[11].tabCase[5][10].coffre=true;

//___________________________________________________


int i,j;
for(i=1;i<6;i++)
{
    for(j=1;j<11;j++)
    {
        tabCarte[0].tabCase[i][j].libre=true;
    }
}
tabCarte[0].tabCase[4][1].libre=false;
tabCarte[0].tabCase[4][2].libre=false;
tabCarte[0].tabCase[4][10].libre=false;

tabCarte[0].tabCase[5][1].libre=false;
tabCarte[0].tabCase[5][2].libre=false;
tabCarte[0].tabCase[5][3].libre=false;
tabCarte[0].tabCase[5][4].libre=false;
tabCarte[0].tabCase[5][5].libre=false;
tabCarte[0].tabCase[5][6].libre=false;
tabCarte[0].tabCase[5][7].libre=false;
tabCarte[0].tabCase[5][8].libre=false;
tabCarte[0].tabCase[5][9].libre=false;
tabCarte[0].tabCase[5][10].libre=false;


tabCarte[0].tabCase[2][11].libre=true;  

tabCarte[0].tabCase[2][11].sortie=true;


//___________________________________________________


tabCarte[1].tabCase[1][1].libre=true;
tabCarte[1].tabCase[1][2].libre=true;
tabCarte[1].tabCase[1][3].libre=true;
tabCarte[1].tabCase[1][4].libre=true;
tabCarte[1].tabCase[1][7].libre=true;
tabCarte[1].tabCase[1][8].libre=true;
tabCarte[1].tabCase[1][9].libre=true;
tabCarte[1].tabCase[1][10].libre=true;

tabCarte[1].tabCase[2][0].libre=true;
tabCarte[1].tabCase[2][1].libre=true;
tabCarte[1].tabCase[2][2].libre=true;
tabCarte[1].tabCase[2][3].libre=true;
tabCarte[1].tabCase[2][4].libre=true;
tabCarte[1].tabCase[2][7].libre=true;
tabCarte[1].tabCase[2][8].libre=true;
tabCarte[1].tabCase[2][9].libre=true;
tabCarte[1].tabCase[2][10].libre=true;

tabCarte[1].tabCase[3][3].libre=true;
tabCarte[1].tabCase[3][8].libre=true;
tabCarte[1].tabCase[3][9].libre=true;
tabCarte[1].tabCase[3][11].libre=true;


tabCarte[1].tabCase[4][4].libre=true;
tabCarte[1].tabCase[4][5].libre=true;
tabCarte[1].tabCase[4][6].libre=true;
tabCarte[1].tabCase[4][8].libre=true;
tabCarte[1].tabCase[4][9].libre=true;
tabCarte[1].tabCase[4][10].libre=true;

tabCarte[1].tabCase[5][1].libre=true;
tabCarte[1].tabCase[5][2].libre=true;
tabCarte[1].tabCase[5][3].libre=true;
tabCarte[1].tabCase[5][4].libre=true;
tabCarte[1].tabCase[5][5].libre=true;
tabCarte[1].tabCase[5][6].libre=true;
tabCarte[1].tabCase[5][7].libre=true;
tabCarte[1].tabCase[5][8].libre=true;
tabCarte[1].tabCase[5][9].libre=true;
tabCarte[1].tabCase[5][10].libre=true;

tabCarte[1].tabCase[6][3].libre=true;

tabCarte[1].tabCase[2][0].sortie=true;
tabCarte[1].tabCase[3][11].sortie=true;
tabCarte[1].tabCase[6][3].sortie=true;

tabCarte[1].tabCase[4][3].libre=true; //case ennemi
tabCarte[1].tabCase[3][10].libre=true; //case ennemi


//___________________________________________________


tabCarte[2].tabCase[1][1].libre=true;
tabCarte[2].tabCase[1][2].libre=true;
tabCarte[2].tabCase[1][3].libre=true;
tabCarte[2].tabCase[1][4].libre=true;
tabCarte[2].tabCase[1][9].libre=true;
tabCarte[2].tabCase[1][10].libre=true;

tabCarte[2].tabCase[2][1].libre=true;
tabCarte[2].tabCase[2][2].libre=true;
tabCarte[2].tabCase[2][3].libre=true;
tabCarte[2].tabCase[2][4].libre=true;
tabCarte[2].tabCase[2][5].libre=true;
tabCarte[2].tabCase[2][6].libre=true;
tabCarte[2].tabCase[2][9].libre=true;
tabCarte[2].tabCase[2][10].libre=true;
tabCarte[2].tabCase[2][11].libre=true;

tabCarte[2].tabCase[3][0].libre=true;
tabCarte[2].tabCase[3][1].libre=true;
tabCarte[2].tabCase[3][2].libre=true;
tabCarte[2].tabCase[3][3].libre=true;
tabCarte[2].tabCase[3][4].libre=true;
tabCarte[2].tabCase[3][5].libre=true;

tabCarte[2].tabCase[4][1].libre=true;
tabCarte[2].tabCase[4][2].libre=true;
tabCarte[2].tabCase[4][3].libre=true;
tabCarte[2].tabCase[4][6].libre=true;
tabCarte[2].tabCase[4][7].libre=true;
tabCarte[2].tabCase[4][8].libre=true;
tabCarte[2].tabCase[4][9].libre=true;

tabCarte[2].tabCase[5][1].libre=true;
tabCarte[2].tabCase[5][6].libre=true;
tabCarte[2].tabCase[5][7].libre=true;
tabCarte[2].tabCase[5][8].libre=true;
tabCarte[2].tabCase[5][9].libre=true;

tabCarte[2].tabCase[2][11].sortie=true;
tabCarte[2].tabCase[3][0].sortie=true;

tabCarte[2].tabCase[3][6].libre=true; //case ennemi
tabCarte[2].tabCase[3][9].libre=true; //case ennemi


//___________________________________________________


tabCarte[3].tabCase[1][1].libre=true;
tabCarte[3].tabCase[1][2].libre=true;
tabCarte[3].tabCase[1][3].libre=true;
tabCarte[3].tabCase[1][4].libre=true;
tabCarte[3].tabCase[1][5].libre=true;
tabCarte[3].tabCase[1][6].libre=true;
tabCarte[3].tabCase[1][7].libre=true;
tabCarte[3].tabCase[1][8].libre=true;
tabCarte[3].tabCase[1][9].libre=true;
tabCarte[3].tabCase[1][10].libre=true;

tabCarte[3].tabCase[2][1].libre=true;
tabCarte[3].tabCase[2][2].libre=true;
tabCarte[3].tabCase[2][3].libre=true;
tabCarte[3].tabCase[2][4].libre=true;
tabCarte[3].tabCase[2][5].libre=true;
tabCarte[3].tabCase[2][6].libre=true;
tabCarte[3].tabCase[2][7].libre=true;
tabCarte[3].tabCase[2][8].libre=true;
tabCarte[3].tabCase[2][9].libre=true;
tabCarte[3].tabCase[2][10].libre=true;

tabCarte[3].tabCase[2][0].libre=true;
tabCarte[3].tabCase[2][1].libre=true;
tabCarte[3].tabCase[2][2].libre=true;
tabCarte[3].tabCase[2][3].libre=true;
tabCarte[3].tabCase[2][4].libre=true;
tabCarte[3].tabCase[2][5].libre=true;
tabCarte[3].tabCase[2][6].libre=true;
tabCarte[3].tabCase[2][7].libre=true;
tabCarte[3].tabCase[2][8].libre=true;
tabCarte[3].tabCase[2][9].libre=true;
tabCarte[3].tabCase[2][10].libre=true;

tabCarte[3].tabCase[4][3].libre=true;
tabCarte[3].tabCase[4][4].libre=true;
tabCarte[3].tabCase[4][9].libre=true;
tabCarte[3].tabCase[4][10].libre=true;

tabCarte[3].tabCase[5][1].libre=true;
tabCarte[3].tabCase[5][2].libre=true;
tabCarte[3].tabCase[5][3].libre=true;
tabCarte[3].tabCase[5][4].libre=true;
tabCarte[3].tabCase[5][5].libre=true;
tabCarte[3].tabCase[5][6].libre=true;
tabCarte[3].tabCase[5][7].libre=true;
tabCarte[3].tabCase[5][8].libre=true;
tabCarte[3].tabCase[5][9].libre=true;
tabCarte[3].tabCase[5][10].libre=true;

tabCarte[3].tabCase[6][2].libre=true;

tabCarte[3].tabCase[2][0].sortie=true;
tabCarte[3].tabCase[6][2].sortie=true;

tabCarte[3].tabCase[3][10].libre=true; //case ennemi


//___________________________________________________


tabCarte[4].tabCase[1][3].libre=true;
tabCarte[4].tabCase[1][4].libre=true;
tabCarte[4].tabCase[1][5].libre=true;
tabCarte[4].tabCase[1][6].libre=true;
tabCarte[4].tabCase[1][7].libre=true;
tabCarte[4].tabCase[1][8].libre=true;
tabCarte[4].tabCase[1][9].libre=true;
tabCarte[4].tabCase[1][10].libre=true;

tabCarte[4].tabCase[2][2].libre=true;
tabCarte[4].tabCase[2][3].libre=true;
tabCarte[4].tabCase[2][4].libre=true;
tabCarte[4].tabCase[2][5].libre=true;
tabCarte[4].tabCase[2][6].libre=true;
tabCarte[4].tabCase[2][7].libre=true;
tabCarte[4].tabCase[2][8].libre=true;
tabCarte[4].tabCase[2][9].libre=true;
tabCarte[4].tabCase[2][10].libre=true;

tabCarte[4].tabCase[3][1].libre=true;
tabCarte[4].tabCase[3][2].libre=true;
tabCarte[4].tabCase[3][3].libre=true;
tabCarte[4].tabCase[3][4].libre=true;
tabCarte[4].tabCase[3][5].libre=true;
tabCarte[4].tabCase[3][6].libre=true;
tabCarte[4].tabCase[3][7].libre=true;
tabCarte[4].tabCase[3][8].libre=true;
tabCarte[4].tabCase[3][9].libre=true;
tabCarte[4].tabCase[3][10].libre=true;

tabCarte[4].tabCase[4][3].libre=true;
tabCarte[4].tabCase[4][4].libre=true;
tabCarte[4].tabCase[4][6].libre=true;
tabCarte[4].tabCase[4][8].libre=true;
tabCarte[4].tabCase[4][9].libre=true;
tabCarte[4].tabCase[4][10].libre=true;
tabCarte[4].tabCase[4][11].libre=true;

tabCarte[4].tabCase[5][9].libre=true;

tabCarte[4].tabCase[6][6].libre=true;

tabCarte[4].tabCase[4][11].sortie=true;
tabCarte[4].tabCase[6][6].sortie=true;

tabCarte[4].tabCase[5][6].libre=true; //case ennemi


//___________________________________________________


tabCarte[5].tabCase[0][3].libre=true;

tabCarte[5].tabCase[1][1].libre=true;
tabCarte[5].tabCase[1][2].libre=true;
tabCarte[5].tabCase[1][4].libre=true;
tabCarte[5].tabCase[1][5].libre=true;
tabCarte[5].tabCase[1][6].libre=true;
tabCarte[5].tabCase[1][8].libre=true;

tabCarte[5].tabCase[2][1].libre=true;
tabCarte[5].tabCase[2][2].libre=true;
tabCarte[5].tabCase[2][3].libre=true;
tabCarte[5].tabCase[2][4].libre=true;
tabCarte[5].tabCase[2][5].libre=true;
tabCarte[5].tabCase[2][6].libre=true;
tabCarte[5].tabCase[2][7].libre=true;
tabCarte[5].tabCase[2][8].libre=true;

tabCarte[5].tabCase[3][1].libre=true;
tabCarte[5].tabCase[3][2].libre=true;
tabCarte[5].tabCase[3][3].libre=true;
tabCarte[5].tabCase[3][4].libre=true;
tabCarte[5].tabCase[3][7].libre=true;
tabCarte[5].tabCase[3][8].libre=true;
tabCarte[5].tabCase[3][9].libre=true;

tabCarte[5].tabCase[4][0].libre=true;
tabCarte[5].tabCase[4][1].libre=true;
tabCarte[5].tabCase[4][2].libre=true;
tabCarte[5].tabCase[4][3].libre=true;
tabCarte[5].tabCase[4][4].libre=true;
tabCarte[5].tabCase[4][7].libre=true;
tabCarte[5].tabCase[4][8].libre=true;
tabCarte[5].tabCase[4][9].libre=true;
tabCarte[5].tabCase[4][10].libre=true;

tabCarte[5].tabCase[5][2].libre=true;
tabCarte[5].tabCase[5][3].libre=true;
tabCarte[5].tabCase[5][4].libre=true;
tabCarte[5].tabCase[5][5].libre=true;
tabCarte[5].tabCase[5][6].libre=true;
tabCarte[5].tabCase[5][7].libre=true;
tabCarte[5].tabCase[5][8].libre=true;
tabCarte[5].tabCase[5][9].libre=true;
tabCarte[5].tabCase[5][10].libre=true;

tabCarte[5].tabCase[6][9].libre=true;

tabCarte[5].tabCase[0][3].sortie=true;
tabCarte[5].tabCase[4][0].sortie=true;
tabCarte[5].tabCase[6][9].sortie=true;

tabCarte[5].tabCase[1][3].libre=true; //case ennemi


//___________________________________________________


tabCarte[6].tabCase[1][1].libre=true;
tabCarte[6].tabCase[1][2].libre=true;
tabCarte[6].tabCase[1][3].libre=true;
tabCarte[6].tabCase[1][4].libre=true;
tabCarte[6].tabCase[1][5].libre=true;
tabCarte[6].tabCase[1][6].libre=true;
tabCarte[6].tabCase[1][7].libre=true;
tabCarte[6].tabCase[1][8].libre=true;
tabCarte[6].tabCase[1][9].libre=true;
tabCarte[6].tabCase[1][10].libre=true;

tabCarte[6].tabCase[2][3].libre=true;
tabCarte[6].tabCase[2][4].libre=true;
tabCarte[6].tabCase[2][5].libre=true;
tabCarte[6].tabCase[2][6].libre=true;
tabCarte[6].tabCase[2][7].libre=true;

tabCarte[6].tabCase[3][4].libre=true;
tabCarte[6].tabCase[3][5].libre=true;
tabCarte[6].tabCase[3][6].libre=true;

tabCarte[6].tabCase[4][1].libre=true;
tabCarte[6].tabCase[4][5].libre=true;
tabCarte[6].tabCase[4][6].libre=true;
tabCarte[6].tabCase[4][7].libre=true;
tabCarte[6].tabCase[4][8].libre=true;

tabCarte[6].tabCase[5][1].libre=true;
tabCarte[6].tabCase[5][2].libre=true;
tabCarte[6].tabCase[5][3].libre=true;
tabCarte[6].tabCase[5][4].libre=true;
tabCarte[6].tabCase[5][5].libre=true;
tabCarte[6].tabCase[5][6].libre=true;
tabCarte[6].tabCase[5][7].libre=true;
tabCarte[6].tabCase[5][8].libre=true;
tabCarte[6].tabCase[5][9].libre=true;
tabCarte[6].tabCase[5][10].libre=true;

tabCarte[6].tabCase[6][6].libre=true;

tabCarte[6].tabCase[6][6].sortie=true;


//___________________________________________________


for(i=1;i<6;i++)
{
    for(j=1;j<11;j++)
    {
        tabCarte[7].tabCase[i][j].libre=true;
    }
}

tabCarte[7].tabCase[1][3].libre=false;
tabCarte[7].tabCase[2][3].libre=false;
tabCarte[7].tabCase[3][3].libre=false;
tabCarte[7].tabCase[3][1].libre=false;
tabCarte[7].tabCase[3][3].libre=false;
tabCarte[7].tabCase[3][4].libre=false;
tabCarte[7].tabCase[3][5].libre=false;
tabCarte[7].tabCase[3][6].libre=false;
tabCarte[7].tabCase[3][7].libre=false;
tabCarte[7].tabCase[3][8].libre=false;
tabCarte[7].tabCase[3][9].libre=false;
tabCarte[7].tabCase[3][10].libre=false;


tabCarte[7].tabCase[0][2].libre=true;
tabCarte[7].tabCase[6][9].libre=true;

tabCarte[7].tabCase[0][2].sortie=true;
tabCarte[7].tabCase[6][9].sortie=true;


//___________________________________________________


tabCarte[8].tabCase[0][6].libre=true;

tabCarte[8].tabCase[1][5].libre=true;
tabCarte[8].tabCase[1][6].libre=true;
tabCarte[8].tabCase[1][7].libre=true;
tabCarte[8].tabCase[1][8].libre=true;

tabCarte[8].tabCase[2][3].libre=true;
tabCarte[8].tabCase[2][4].libre=true;
tabCarte[8].tabCase[2][5].libre=true;
tabCarte[8].tabCase[2][6].libre=true;
tabCarte[8].tabCase[2][7].libre=true;
tabCarte[8].tabCase[2][8].libre=true;

tabCarte[8].tabCase[3][3].libre=true;
tabCarte[8].tabCase[3][4].libre=true;
tabCarte[8].tabCase[3][5].libre=true;
tabCarte[8].tabCase[3][6].libre=true;
tabCarte[8].tabCase[3][7].libre=true;
tabCarte[8].tabCase[3][8].libre=true;
tabCarte[8].tabCase[3][9].libre=true;
tabCarte[8].tabCase[3][10].libre=true;
tabCarte[8].tabCase[3][11].libre=true;

tabCarte[8].tabCase[4][3].libre=true;
tabCarte[8].tabCase[4][4].libre=true;
tabCarte[8].tabCase[4][5].libre=true;
tabCarte[8].tabCase[4][8].libre=true;
tabCarte[8].tabCase[4][9].libre=true;

tabCarte[8].tabCase[5][3].libre=true;
tabCarte[8].tabCase[5][8].libre=true;
tabCarte[8].tabCase[5][9].libre=true;
tabCarte[8].tabCase[5][10].libre=true;

tabCarte[8].tabCase[0][6].sortie=true;
tabCarte[8].tabCase[3][11].sortie=true;

tabCarte[8].tabCase[4][2].libre=true; //case ennemi


//___________________________________________________
//zone départ
//point début: [2][5]

tabCarte[9].tabCase[0][9].libre=true;

tabCarte[9].tabCase[1][4].libre=true;
tabCarte[9].tabCase[1][5].libre=true;
tabCarte[9].tabCase[1][6].libre=true;
tabCarte[9].tabCase[1][7].libre=true;
tabCarte[9].tabCase[1][10].libre=true;

tabCarte[9].tabCase[2][4].libre=true;
tabCarte[9].tabCase[2][5].libre=true;
tabCarte[9].tabCase[2][6].libre=true;
tabCarte[9].tabCase[2][7].libre=true;
tabCarte[9].tabCase[2][8].libre=true;
tabCarte[9].tabCase[2][9].libre=true;
tabCarte[9].tabCase[2][10].libre=true;

tabCarte[9].tabCase[3][0].libre=true;
tabCarte[9].tabCase[3][1].libre=true;
tabCarte[9].tabCase[3][4].libre=true;
tabCarte[9].tabCase[3][5].libre=true;
tabCarte[9].tabCase[3][6].libre=true;
tabCarte[9].tabCase[3][7].libre=true;
tabCarte[9].tabCase[3][8].libre=true;
tabCarte[9].tabCase[3][9].libre=true;
tabCarte[9].tabCase[3][10].libre=true;

tabCarte[9].tabCase[4][4].libre=true;
tabCarte[9].tabCase[4][5].libre=true;
tabCarte[9].tabCase[4][6].libre=true;
tabCarte[9].tabCase[4][7].libre=true;
tabCarte[9].tabCase[4][8].libre=true;
tabCarte[9].tabCase[4][9].libre=true;
tabCarte[9].tabCase[4][10].libre=true;

tabCarte[9].tabCase[5][6].libre=true;
tabCarte[9].tabCase[5][7].libre=true;
tabCarte[9].tabCase[5][8].libre=true;
tabCarte[9].tabCase[5][9].libre=true;
tabCarte[9].tabCase[5][10].libre=true;

tabCarte[9].tabCase[0][9].sortie=true;
tabCarte[9].tabCase[3][0].sortie=true;

tabCarte[9].tabCase[1][9].libre=true; //case ennemi

//___________________________________________________


tabCarte[10].tabCase[0][6].libre=true;

tabCarte[10].tabCase[1][1].libre=true;
tabCarte[10].tabCase[1][2].libre=true;
tabCarte[10].tabCase[1][5].libre=true;
tabCarte[10].tabCase[1][6].libre=true;
tabCarte[10].tabCase[1][7].libre=true;
tabCarte[10].tabCase[1][8].libre=true;
tabCarte[10].tabCase[1][9].libre=true;
tabCarte[10].tabCase[1][10].libre=true;

tabCarte[10].tabCase[2][1].libre=true;
tabCarte[10].tabCase[2][2].libre=true;
tabCarte[10].tabCase[2][5].libre=true;
tabCarte[10].tabCase[2][6].libre=true;
tabCarte[10].tabCase[2][7].libre=true;
tabCarte[10].tabCase[2][8].libre=true;

tabCarte[10].tabCase[3][6].libre=true;
tabCarte[10].tabCase[3][10].libre=true;
tabCarte[10].tabCase[3][11].libre=true;

tabCarte[10].tabCase[4][1].libre=true;
tabCarte[10].tabCase[4][2].libre=true;
tabCarte[10].tabCase[4][3].libre=true;
tabCarte[10].tabCase[4][6].libre=true;
tabCarte[10].tabCase[4][7].libre=true;
tabCarte[10].tabCase[4][9].libre=true;
tabCarte[10].tabCase[4][10].libre=true;

tabCarte[10].tabCase[5][1].libre=true;
tabCarte[10].tabCase[5][2].libre=true;
tabCarte[10].tabCase[5][3].libre=true;
tabCarte[10].tabCase[5][4].libre=true;
tabCarte[10].tabCase[5][5].libre=true;
tabCarte[10].tabCase[5][6].libre=true;
tabCarte[10].tabCase[5][7].libre=true;
tabCarte[10].tabCase[5][8].libre=true;
tabCarte[10].tabCase[5][9].libre=true;
tabCarte[10].tabCase[5][10].libre=true;

tabCarte[10].tabCase[0][6].sortie=true;
tabCarte[10].tabCase[3][11].sortie=true;

tabCarte[10].tabCase[3][1].libre=true; //case ennemi

//___________________________________________________

tabCarte[11].tabCase[0][9].libre=true;

tabCarte[11].tabCase[1][1].libre=true;
tabCarte[11].tabCase[1][2].libre=true;
tabCarte[11].tabCase[1][3].libre=true;
tabCarte[11].tabCase[1][6].libre=true;
tabCarte[11].tabCase[1][7].libre=true;
tabCarte[11].tabCase[1][8].libre=true;
tabCarte[11].tabCase[1][9].libre=true;
tabCarte[11].tabCase[1][10].libre=true;

tabCarte[11].tabCase[2][1].libre=true;
tabCarte[11].tabCase[2][2].libre=true;
tabCarte[11].tabCase[2][3].libre=true;
tabCarte[11].tabCase[2][6].libre=true;
tabCarte[11].tabCase[2][7].libre=true;
tabCarte[11].tabCase[2][8].libre=true;
tabCarte[11].tabCase[2][9].libre=true;
tabCarte[11].tabCase[2][10].libre=true;

tabCarte[11].tabCase[3][0].libre=true;
tabCarte[11].tabCase[3][1].libre=true;
tabCarte[11].tabCase[3][2].libre=true;
tabCarte[11].tabCase[3][3].libre=true;
tabCarte[11].tabCase[3][4].libre=true;
tabCarte[11].tabCase[3][7].libre=true;
tabCarte[11].tabCase[3][8].libre=true;

tabCarte[11].tabCase[4][1].libre=true;
tabCarte[11].tabCase[4][2].libre=true;
tabCarte[11].tabCase[4][3].libre=true;
tabCarte[11].tabCase[4][4].libre=true;
tabCarte[11].tabCase[4][6].libre=true;
tabCarte[11].tabCase[4][7].libre=true;
tabCarte[11].tabCase[4][10].libre=true;

tabCarte[11].tabCase[5][2].libre=true;
tabCarte[11].tabCase[5][3].libre=true;
tabCarte[11].tabCase[5][4].libre=true;
tabCarte[11].tabCase[5][5].libre=true;
tabCarte[11].tabCase[5][7].libre=true;

tabCarte[11].tabCase[0][9].sortie=true;
tabCarte[11].tabCase[3][0].sortie=true;

tabCarte[11].tabCase[5][6].libre=true; //case ennemi
tabCarte[11].tabCase[3][10].libre=true; //case ennemi

}


int Monde::getX(){
    return mX;
}

int Monde::getY(){
    return mY;
}

void Monde::setX(const int& nvX){
    mX=nvX;
}

void Monde::setY(const int& nvY){
    mY=nvY;
}

int Monde::getCarte() const{
    return mCarte;
}

void Monde::setCarte(const int& nvCarte){
    mCarte=nvCarte;
}