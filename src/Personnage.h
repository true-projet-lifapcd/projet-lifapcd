#ifndef __PERSONNAGE__H
#define __PERSONNAGE__H

/**
 * @struct Personnage
 *
 * @brief Détermine les statistiques d'un personnage
 **/
struct Personnage {
	int vie;
	int vieMax;
	int atkBase;
	int def;
	int energie;
};

#endif
